#+title: IE University Formula One Resources

* Road Map
Gather information on:
+ [ ] Competition rules
+ [ ] Competition tracks
+ [ ] Competition requirements
+ [ ] Previous Cars
+ [ ] Main mistakes from last years

* Preparation
+ Getting Started :: [[./Preparation/PROCESS FOR WORK.pdf]]

** Design
+ [[./Preparation/DesignofFormulaStudentVehicle_1.1-29.pdf]]

* HOWTO
+ Create a gitlab account
+ Send me your username
